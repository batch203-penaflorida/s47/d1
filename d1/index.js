// console.log("Hello world");

// SECTION Document Object Model

// Allows us to be able to access or modify the properties of an html element in a web page.

// It is a standard on how to get, change, add, or delete HTML elements.

// We will focus on using DOM for managing forms.

// For selecting HTML elements will be using the document.querySelector("");

// Syntax: document.querySelector("htmlElement");

// document refers to the whole page
// ".querySelector" is used to select a specific object (HTML elements) from the document (web page).

// The query selector function takes a string input that is formatted like

// Alternatively, we can use the getElement fucntions to retrieve the elements.
// document.getElementById("txt-first-name");

// However, using these functions require us to identify beforehand how we get the elements. With query selector, we can be flexible in how to retrieve the elements.
const txtFirstName = document.querySelector("#txt-first-name");

const txtLastName = document.querySelector("#txt-last-name");

const spanFullName = document.querySelector("#span-full-name");

const changeColor = document.querySelector("#text-color");

const container = document.querySelector(".container");

// [SECTION] Event Listeners
// Whenever a user interacts with a web page, this action is considered as an event.
// Working with events is large part of creating interactivity in a web page.
// To perform an action when an event triggers, you first need to listen to it.

// The function use is "addEventListnener" that takes two arguments
// A string identifying an event;
// and a function that the listener will execute once the "specified event" is trigggered.
// When an event occurs, an "event object" is passed to the function argument as the first parameter.
txtFirstName.addEventListener("keyup", (e) => {
  console.log(e.target);
  console.log(e.target.value);
});

txtLastName.addEventListener("keyup", (e) => {
  console.log(e.target);
  console.log(e.target.value);
});

const fullName = () => {
  spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`;
};

txtFirstName.addEventListener("keyup", fullName);
txtLastName.addEventListener("keyup", fullName);

changeColor.addEventListener("click", (e) => {
  if (e.target.value === "red") {
    spanFullName.style.color = "#F7665E";
    changeColor.style.backgroundColor = "#F7665E";
    changeColor.style.color = "white";
    container.classList.add = "red";
  } else if (e.target.value === "blue") {
    spanFullName.style.color = "#5284E7";
    changeColor.style.backgroundColor = "#5284E7";
    changeColor.style.color = "white";
  } else if (e.target.value === "green") {
    console.log(e.target.value);
    spanFullName.style.color = "#18A34A";
    changeColor.style.backgroundColor = "#18A34A";
    changeColor.style.color = "white";
  } else {
    spanFullName.style.color = "black";
  }
});
